import os
import sys

import pandas
import logging


class Parser:
    @staticmethod
    def xls_to_csv(xls_path, csv_path, columns: dict):
        data = pandas.read_excel(
            xls_path,
            dtype=columns,
            usecols=list(columns.keys())
        )
        logging.info('Dataframe has been loaded')
        logging.info(data)
        data = data.dropna()
        logging.info('NaNs has been dropped')
        logging.info(data)
        data = data.round(decimals=2)
        logging.info('Numerics has been rounded to 2 decimals')
        logging.info(data)
        data.index.name = 'index'
        logging.info('Index name has been changed')
        logging.info(data)
        if os.path.exists(csv_path):
            os.remove(csv_path)
            logging.info('Old test file has been removed')
        elif not os.path.exists(os.path.dirname(csv_path)):
            os.mkdir(os.path.dirname(csv_path))
        data.to_csv(csv_path, index=None)
        logging.info('New CSV has been created')

    @staticmethod
    def get_matrix(path):
        return pandas.read_csv(path).to_numpy()

    @staticmethod
    def get_number(path: str) -> float:
        if os.path.exists(path):
            with open(path, 'r') as state:
               return float(state.read().strip())
        return sys.float_info.max

    @staticmethod
    def write_number(path: str, number: float):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w+') as state:
            state.write(str(number))

import logging

from parser_utils.parser import Parser


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    Parser.xls_to_csv(
        'data/sosna_kora-v1_0.7.xlsx',
        'data/sosna_kora-valid.csv',
        {'l_drzewa': 'float', 'Miejsce_po': 'float', 'avg_d': 'float', 'k1': 'float', 'k2': 'float', 'zależna': 'float'}
    )
    Parser.xls_to_csv(
        'data/sosna_kora-v1_0.7.xlsx',
        'data/sosna_kora-train.csv',
        {'l_drzewa': 'float', 'Miejsce_po': 'float', 'avg_d': 'float'}
    )

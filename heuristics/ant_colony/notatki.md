Istotna czesc algorytmu mrowkowego dla klasy ANT.
Wskazuje jak należy dobierać kolejny ruch, tzn. 
w przypadku sieci neuronowej liczbę unitów na warstwie.
W przypadku wyboru liczby warstw będzie to po prostu 
pojedyncze losowanie, definiujące liczbę losowań unitow.

```python
import random

def move(self, start, alpha, beta):
        self.__tour.append(start)
        self.__unvisited.remove(start)

        current_row = start
        while self.__unvisited.__len__() > 0:
            pheromone_sum = 0
            probability = []

            for place in self.__unvisited:
                cost = pow(1 / self.__maps[current_row][place][0], alpha)
                pheromone = pow(self.__maps[current_row][place][1], beta)
                probability.append(cost * pheromone)
                pheromone_sum += cost * pheromone

            for i in range(probability.__len__()):
                probability[i] /= pheromone_sum

            chosen = random.choices(self.__unvisited, probability)
            self.__cost += self.__maps[self.__tour[-1]][chosen[0]][0]
            self.__tour.append(chosen[0])
            self.__unvisited.remove(chosen[0])

            current_row = chosen[0]

        self.__cost += self.__maps[self.__tour[-1]][self.__tour[0]][0]
        self.__tour.append(self.__tour[0])
```

Dla sieci neuronowej musimy mieć zdefiniowane:
- Max liczba layerów
- Max liczba unitów

Przeszukiwanie odbywa się dwuwymiarowo, czyli dobieramy liczbę warstw, 
a później liczbę unitów na każdej warstwie.

Parametr `alpha` i `beta` oznaczają kolejno skalowanie dla wagi 
kosztu oraz wagi feromonów. Powstały w celu uwzględnienia wyniku
przy wyliczaniu ilości foromonów w ścieżce.

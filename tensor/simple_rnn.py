import os
import sys
import logging
import numpy as np
from typing import List, Tuple
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from tensorflow import keras
from parser_utils.parser import Parser
from resources.resource import Resource

logging.basicConfig(level=logging.INFO)


class SimpleRnn:
    @staticmethod
    def run(time_steps: int, epochs: int, units: List[int]) -> Tuple[float, List[int]]:
        matrix = Parser.get_matrix(Resource.path('sosna_kora-valid.csv'))
        if time_steps > int(len(matrix) / 2):
            raise TypeError('<timesteps> value should be less than half of number of data chunks')
        if epochs < 10:
            raise TypeError('<epoches> value should not be less than 10')
        if len(units) < 1:
            raise TypeError('<units> list should contains any value')
        logging.info('Provided parameters: timesteps: %s, epochs: %s, units: %s', sys.argv[1], sys.argv[2], sys.argv[3:])

        x_train = []
        y_train = []
        for i in range(len(matrix)):
            x_train.append(matrix[i][0]-matrix[i][1])
            x_train.append(matrix[i][2])
            y_train.append(matrix[i][3])
            y_train.append(matrix[i][4])

        features = 2
        size_x = int(len(x_train) / (features * time_steps))
        size_y = int(len(y_train) / (features * time_steps))

        x_train = x_train[:(size_x * time_steps * features)]
        y_train = y_train[:(size_y * time_steps * features)]

        x_train = np.array(x_train)
        x_train = np.reshape(x_train, (size_x, time_steps, features))
        y_train = np.array(y_train)
        y_train = np.reshape(y_train, (size_y, time_steps, features))

        model = keras.Sequential()
        for unit in units:
            model.add(keras.layers.SimpleRNN(int(unit), input_shape=(None, 2), activation='sigmoid', return_sequences=True))
        model.add(keras.layers.Dense(2, activation='relu'))
        model.compile(optimizer=keras.optimizers.Adam(), loss=keras.losses.MeanSquaredError(), metrics=[keras.metrics.MeanAbsoluteError()])
        history = model.fit(x_train, y_train, epochs=epochs, verbose=0)

        loss = min(history.history['loss'])
        logging.info('Lower loss for current architecture: %f', loss)
        path = '../output'
        if loss <= Parser.get_number(path + '/best_loss'):
            logging.info('Current loss is lower than actual - saving...')
            Parser.write_number(path + '/best_loss', loss)
            keras.utils.plot_model(model, to_file=(path + '/model.png'), show_shapes=True)
            model.save(path + '/best.h5')
        return loss, units

    @staticmethod
    def run_n_times(times: int, params: Tuple[int, int, List[int]]) -> Tuple[float, List[int]]:
        best = (sys.float_info.max, [])
        for _ in range(times):
            loss, units = SimpleRnn.run(params[0], params[1], params[2])
            if best[0] >= loss:
                best = (loss, units)
        return best


if __name__ == '__main__':
    bestie = SimpleRnn.run_n_times(10, (int(sys.argv[1]), int(sys.argv[2]), [int(x) for x in sys.argv[3:]]))
    logging.info('Best loss: %s', bestie[0])
